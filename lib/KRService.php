<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class KRService extends Sogou_Api_Client_Core{
	public function __construct() {
		parent::__construct('KRService');
	}
}

$service = new KRService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getKRbySeedWords function
$arguments = array('getKRbySeedWordsRequest' => array('seedWords' => '鲜花', 'filterType' => 
		array ('maxNum' => 20)));
$output_response = $service->soapCall('getKRbySeedWords', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
