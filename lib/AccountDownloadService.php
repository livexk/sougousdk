<?php


namespace sougousdk;
require_once 'Sogou_API_Core.php';

class AccountDownloadService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('AccountDownloadService');
	}
}

$service = new AccountDownloadService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getAccountFile function
$arguments = array('getAccountFileRequest' => array('accoutFileRequest' => array('includeTemp'=>true, 'includeQuality' => false, 'format' =>'1')));
$output_response = $service->soapCall('getAccountFile', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);

