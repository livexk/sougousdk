<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class CpcIdeaService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('CpcIdeaService');
	}
}

$service = new CpcIdeaService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getCpcIdeaByCpcGrpId function
$arguments = array('getCpcIdeaByCpcGrpIdRequest' => array('getTemp' => 1, 'cpcGrpIds' => 
		array (54204009)));
$output_response = $service->soapCall('getCpcIdeaByCpcGrpId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
