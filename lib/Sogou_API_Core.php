<?php
/**
 * 
 * @author Sogou API Team
 *
 */

// Sandbox URL
define('URL', 'http://apisandbox.agent.sogou.com:8080');
// Online URL
//define('URL', 'http://api.agent.sogou.com:8080');


//for agent, if you are not agent, comment following two lines
define('AGENTUSERNAME', '');
define('AGENTPASSWORD', '');

// for user


class Sogou_Api_Client_Core {
	private $soapClient;
	

	/**
	 * construcor of Sogou_Api_Client_Core, only need the service name.
	 * @param String $serviceName
	 */
	public function __construct($serviceName) {

		$this->soapClient = new SoapClient ( URL . '/sem/sms/v1/' . $serviceName . '?wsdl', array ('trace' => TRUE, 'connection_timeout' => 30 ) );
		// set user's soapheader
		//$sh_param = array ('username' =>  USERNAME, 'password' =>  PASSWORD, 'token' =>  TOKEN );
		// set agent's soapheader 
		$sh_param = array ('agentusername' => AGENTUSERNAME, 'agentpassword' => AGENTPASSWORD, 'username' =>  USERNAME, 'password' =>  PASSWORD, 'token' =>  TOKEN );
		var_dump($sh_param);
		$headers = new SoapHeader ( 'http://api.sogou.com/sem/common/v1', 'AuthHeader', $sh_param );
		var_dump($headers);
		// Prepare Soap Client 
		$this->soapClient->__setSoapHeaders ( array ( $headers ) );
	}
	

	public function getFunctions() {
		return $this->soapClient->__getFunctions();
	}
	
	public function getTypes() {
		return $this->soapClient->__getTypes();
	}
	
	public function soapCall($function_name, array $arguments, array &$output_headers) {
		return $this->soapClient->__soapCall($function_name, $arguments, null, null, $output_headers);
	}
}
