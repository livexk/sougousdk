
Sogou API PHP客户端说明：

此Sogou API PHP客户端代码仅作为示范，适用于熟悉PHP但是不熟悉WebService Client开发的程序员。
本示例代码旨在为程序员提供参考和示范，每个服务仅提供了部分接口调用，如果需要了解所有的服务和各个服务的接口参数说明，
请查询完整的线上开发文档：
http://apihome.sogou.com/docs.jsp


注意事项:

1. 如何设置SOAP Header ?
	参见Sogou_API_Core.php。
		1)开通API权限的账户是普通用户：用户名密码在username和password中设置
		2)开通API权限的账户是代理商：agentusername和agentpassword中设置自己的账户信息，
		username和password则是被代管的普通广告用户

2. 本客户端的运行环境
	PHP 5.3.3 (cli) 

Copyright 2011-2012 Sogou.com Inc.
Sogou API Team
