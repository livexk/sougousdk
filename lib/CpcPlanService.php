<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class CpcPlanService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('CpcPlanService');
	}
}

$service = new CpcPlanService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getAllCpacPlanId function
$arguments = array('getAllCpcPlanIdRequest' => array());
$output_response = $service->soapCall('getAllCpcPlanId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);

// Call getCpcPlanByCpcPlanId function
$arguments = array('getCpcPlanByCpcPlanIdRequest' => array('cpcPlanIds' => array(341395, 341332)));
$output_response = $service->soapCall('getCpcPlanByCpcPlanId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
