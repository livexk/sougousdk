<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class ReportService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('ReportService');
	}
}

$service = new ReportService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getReportId function
$arguments = array('getReportIdRequest' => array('reportRequestType' => 
		array ('performanceData' => array('cost', 'cpc', 'click', 'impression', 'ctr'), 'levelOfDetails' => 3, 
		'reportType' => 3, 'startDate' => '2012-03-22T00:00:00', 'endDate' => '2012-05-22T00:00:00')));
$output_response = $service->soapCall('getReportId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
