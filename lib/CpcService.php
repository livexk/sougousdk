<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class CpcService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('CpcService');
	}
}

$service = new CpcService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getCpcByCpcGrpId function
$arguments = array('getCpcByCpcGrpIdRequest' => array('getTemp' => 1, 'cpcGrpIds' => 
		array (54204009)));
$output_response = $service->soapCall('getCpcByCpcGrpId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
