<?php
namespace sougousdk;

require_once 'Sogou_API_Core.php';

class CpcGrpService extends Sogou_Api_Client_Core {
	public function __construct() {
		parent::__construct('CpcGrpService');
	}
}

$service = new CpcGrpService();
$output_headers = array();

// Show service definition. 
print('----------service types-----------');
print_r($service->getTypes());
print('----------service functions-----------');
print_r($service->getFunctions());
print("----------service end-----------\n");

// Call getAllCpcPlanId function
$arguments = array('getAllCpcGrpIdRequest');

$output_response = $service->soapCall('getAllCpcGrpId', $arguments, $output_headers);
print('----------output body-----------');
print_r($output_response);
print('----------output header-----------');
print_r($output_headers);
